import React, { Component } from 'react';

interface IProps {
    listOfPeople: any[]
}

interface MyState {
    clicked: boolean
}
export class DashBoard extends Component<IProps, MyState> {

    constructor(props:IProps){
        super(props)

        this.state = {
            clicked: false
        }

    }

    render() {
        return (
            <div style={{border: this.state.clicked ? "1 px solid red" : "0px solid red"}}>
                <p>this is the dashboard</p>
            </div>
        );
    }
}
