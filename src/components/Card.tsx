import React, { Component } from 'react';


interface IProps {
    selected: Boolean,
    name: String,
    description: String

}

export class Card extends Component<IProps, {}> {

    constructor(props:IProps){
        super(props)

    }

    render() {
        return (
            <div style={this.props.selected ? styles.border : styles.NotSelectedborder}>
                <h1>{this.props.name}</h1>
                <p>{this.props.description}</p>
            </div>
        );
    }
}


const styles = {
    border: {
        border: "1px dotted green"
    },
    NotSelectedborder: {
        border: "1px dotted black"
    }
}
