import React, { Component } from 'react';
import { Persona, PersonaSize } from 'office-ui-fabric-react/lib/Persona';
import { Stack, IStackTokens } from 'office-ui-fabric-react/lib/Stack';


interface Iprops {
    listOfPeople: any[],
    onClick: any
}

interface MyState {
    showItems:number
}

const stackTokens: IStackTokens = { childrenGap: 5, maxHeight:50 };


export class HeaderBar extends Component< Iprops, MyState> {
    constructor(props:Iprops){
        super(props)

        this.state = {
            showItems: 3
        }
    }

    getInitialsOfName = (name:string): string => {
        var fName = name.split(' ').slice(0, -1).join(' ');
        var lName = name.split(' ').slice(-1).join(' ');

        return `${fName[0] + lName[0]}`
    }

    render():JSX.Element {
        return (
            <Stack horizontal verticalAlign={"center"} tokens={stackTokens} style={{width:500, backgroundColor:"black"}}>
                <Stack horizontal horizontalAlign={"start"}>
                    {
                        this.props.listOfPeople.slice(0, this.state.showItems).map( (person:any) => {
                            return (
                                <Persona
                                    imageInitials={this.getInitialsOfName(person.name)}
                                    size={PersonaSize.size32}
                                    onClick={() => {this.props.onClick(person.key)}}
                                    key={"PersonaHeader: "+person.key}
                                />
                            )
                        })
                    }
                </Stack>
                <Stack.Item align={"end"}>
                    <p style={{color:"orange"}}>ald</p>
                </Stack.Item>
            </Stack>
        )
    }
}
