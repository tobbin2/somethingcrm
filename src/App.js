import React, {Component} from 'react';
import { HeaderBar } from './components/HeaderBar.tsx'
import { DashBoard } from './components/DashBoard'
import { Card } from './components/Card'
let dummyData = [ {name:"Robin Sauma", key:0}, {name: "Fahed Yousif", key:1}, {name: "Khaleel Elias", key:2}]

class App extends Component {

  constructor(){
    super()

    this.state = {
      selectedList: {}
    }

  }

  handleClickedHeaderPeople = (key) => {
    this.state.selected[key].selected = !this.state.selected[key].selected
  }

  render() {
    return (
      <div>
        <HeaderBar listOfPeople={dummyData} onClick={(key) => { console.log("key: ", key , " clicked!")}} />
        {
          dummyData.map( (value) => {
           
            return <Card
              selected={false}
              name={value.name}
              description={"desc"}
            />
          })
        }
        <DashBoard listOfPeople={dummyData} />
      </div>
    );
  }
}

export default App;
